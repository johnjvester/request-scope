# `request-scope`

> Demo of using @RequestScope for `RequestData` set during login process, as inspired by a recent article on DZone.com.

## Overview

The `request-scope` repository contains a Spring Boot application intended to demonstrate how a request scope bean can 
be established inside an interceptor during the initial request, allowing the contents of that bean to be available 
further down the request lifecycle.

## Using `request-scope` 

Simply start the application by running the `Application` class in your favorite Spring Boot-enabled IDE.  

Once the server is running, the following URI will be available:

```
http://<server:port location>/sample/{accountId}?ticketId={ticketId}
```

The `{accountId}` value is expected to be a numeric value and the `{ticketId}` is a string.  Since this is a simple example, 
entering anything else, or any extra items onto the requestURI will result in 5xx error.

As an example, the following request:

```
http://localhost:8080/sample/1234?ticketId=998877665544332211
```

Will result in an `application/json` response similar to what is displayed below:

```
{ 
   "accountId":1234,
   "accountName":"Some Account Name",
   "accountManager":"john.doe",
   "accountManagerId":"20a1b4fa-322f-4243-a598-03b9946c0ba0"
}
```

## Request Flow

The following bullet-points describe the request lifecycle:

1. The client makes a valid request (i.e. `http://localhost:8080/sample/1234?ticketId=998877665544332211`)
1. The `WebConfig` determines if the pattern for the request matches an Interceptor (`/sample/` is a match)
1. The `WebConfig` also knows about the `RequestData` `@Bean` which is a `@RequestScope` object
1. The `SecurityInterceptor` sets the `Account` and `User` objects and adds them to the `RequestData` object
1. Control is returned to the `SampleController` which then calls the `SampleService.doSomething()` method
1. The `SampleServiceImpl` class is utilized to run the `doSomething()` method.
1. Since the `SampleServiceImpl` class includes the `RequestData` object, the values are available for use
1. The `doSometing()` method converts the values of the `Account` and `User` objects into a `SampleResponse` object, which is returned to the client as an `application/json` payload

## Additional Information

Please review the following DZone.com article for more details:

[Using @RequestScope With Your API](https://dzone.com/articles/using-requestscope-with-your-api)

Made with ♥ by John Vester.