package com.gitlab.johnjvester.requestscope.services;

import com.gitlab.johnjvester.requestscope.entities.Account;
import com.gitlab.johnjvester.requestscope.entities.User;
import com.gitlab.johnjvester.requestscope.services.impl.SampleServiceImpl;
import com.gitlab.johnjvester.requestscope.web.interceptor.RequestData;
import com.gitlab.johnjvester.requestscope.web.models.SampleResponse;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.RequestScope;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@Slf4j
@ContextConfiguration
@WebAppConfiguration
@SpringBootTest
public class SampleServiceTest {
    @Autowired
    GenericApplicationContext context;

    @Autowired
    MockHttpServletRequest request;

    @MockBean
    RequestData requestData;

    @InjectMocks
    SampleServiceImpl sampleService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        context.getBeanFactory().registerScope(WebApplicationContext.SCOPE_REQUEST, new RequestScope());
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    }

    @Test
    public void testDoSomething() {
        Account account = new Account();
        account.setId(101L);
        account.setName("name");

        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setTicketId("NotARealTicketId");
        user.setUserName("userName");

        when(requestData.getUser()).thenReturn(user);
        when(requestData.getAccount()).thenReturn(account);

        SampleResponse sampleResponse = new SampleResponse();
        sampleResponse.setAccountId(account.getId());
        sampleResponse.setAccountName(account.getName());
        sampleResponse.setAccountManagerId(user.getId());
        sampleResponse.setAccountManager(user.getUserName());

        SampleResponse testSampleResponse = sampleService.doSomething();

        assertEquals(sampleResponse.getAccountId(), testSampleResponse.getAccountId());
        assertEquals(sampleResponse.getAccountName(), testSampleResponse.getAccountName());
        assertEquals(sampleResponse.getAccountManagerId(), testSampleResponse.getAccountManagerId());
        assertEquals(sampleResponse.getAccountManager(), testSampleResponse.getAccountManager());
    }
}
