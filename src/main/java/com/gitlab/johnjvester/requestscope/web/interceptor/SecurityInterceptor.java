package com.gitlab.johnjvester.requestscope.web.interceptor;

import com.gitlab.johnjvester.requestscope.entities.Account;
import com.gitlab.johnjvester.requestscope.entities.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Slf4j
public class SecurityInterceptor extends HandlerInterceptorAdapter {
    @Resource(name = "requestScopeRequestData")
    private RequestData requestData;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String ticketId = request.getParameter("ticketId");
        log.info("ticketId={}", ticketId);

        Long accountId = getAccountIdFromRequestUri(request.getRequestURI());

        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setUserName("john.doe");
        user.setTicketId(ticketId);

        Account account = new Account();
        account.setId(accountId);
        account.setName("Some Account Name");

        requestData.setUser(user);
        requestData.setAccount(account);

        return true;
    }

    private Long getAccountIdFromRequestUri(String requestUri) {
        log.info("getAccountIdFromRequestUri(requestUri={})", requestUri);

        int accountIdStart = StringUtils.indexOf(requestUri, "/sample/") + 8;

        String accountIdString = StringUtils.substring(requestUri, accountIdStart);
        log.info("accountIdString={}", accountIdString);

        return Long.valueOf(accountIdString);
    }
}
