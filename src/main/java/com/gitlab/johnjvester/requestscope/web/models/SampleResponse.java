package com.gitlab.johnjvester.requestscope.web.models;

import lombok.Data;

@Data
public class SampleResponse {
    private long accountId;
    private String accountName;
    private String accountManager;
    private String accountManagerId;
}
