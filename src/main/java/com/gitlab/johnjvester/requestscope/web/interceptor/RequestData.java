package com.gitlab.johnjvester.requestscope.web.interceptor;

import com.gitlab.johnjvester.requestscope.entities.Account;
import com.gitlab.johnjvester.requestscope.entities.User;
import lombok.Data;

@Data
public class RequestData {
    private User user;
    private Account account;
}
