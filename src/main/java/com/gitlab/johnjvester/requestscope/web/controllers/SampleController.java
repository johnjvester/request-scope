package com.gitlab.johnjvester.requestscope.web.controllers;

import com.gitlab.johnjvester.requestscope.services.SampleService;
import com.gitlab.johnjvester.requestscope.web.models.SampleResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@RequiredArgsConstructor
@Slf4j
@CrossOrigin
@Controller
@RequestMapping(value = "/sample", produces = MediaType.APPLICATION_JSON_VALUE)
public class SampleController {
    private final SampleService sampleService;

    @ResponseBody
    @GetMapping(value = "/{accountId}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<SampleResponse> doSomething(@PathVariable Long accountId, @RequestParam String ticketId) {
        return new ResponseEntity<>(sampleService.doSomething(), HttpStatus.OK);
    }
}
