package com.gitlab.johnjvester.requestscope.config;

import com.gitlab.johnjvester.requestscope.web.interceptor.RequestData;
import com.gitlab.johnjvester.requestscope.web.interceptor.SecurityInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Bean
    public SecurityInterceptor getSecurityInterceptor() {
        return new SecurityInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getSecurityInterceptor()).addPathPatterns("/sample/**");
    }

    @Bean
    @RequestScope
    public RequestData requestScopeRequestData() {
        return new RequestData();
    }
}
