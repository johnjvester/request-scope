package com.gitlab.johnjvester.requestscope.entities;

import lombok.Data;

@Data
public class Account {
    private long id;
    private String name;
}
