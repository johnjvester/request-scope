package com.gitlab.johnjvester.requestscope.entities;

import lombok.Data;

@Data
public class User {
    private String id;
    private String userName;
    private String ticketId;
}
