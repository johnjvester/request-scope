package com.gitlab.johnjvester.requestscope.services.impl;

import com.gitlab.johnjvester.requestscope.services.SampleService;
import com.gitlab.johnjvester.requestscope.web.interceptor.RequestData;
import com.gitlab.johnjvester.requestscope.web.models.SampleResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Slf4j
@Service
public class SampleServiceImpl implements SampleService {
    @Resource(name = "requestScopeRequestData")
    private RequestData requestData;

    @Override
    public SampleResponse doSomething() {
        SampleResponse sampleResponse = new SampleResponse();
        sampleResponse.setAccountName(requestData.getAccount().getName());
        sampleResponse.setAccountId(requestData.getAccount().getId());
        sampleResponse.setAccountManager(requestData.getUser().getUserName());
        sampleResponse.setAccountManagerId(requestData.getUser().getId());

        log.info("sampleResponse={}", sampleResponse);
        return sampleResponse;
    }
}
