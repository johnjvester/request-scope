package com.gitlab.johnjvester.requestscope.services;

import com.gitlab.johnjvester.requestscope.web.models.SampleResponse;

public interface SampleService {
    SampleResponse doSomething();
}
